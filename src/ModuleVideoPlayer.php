<?php

namespace TMCms\Modules\VideoPlayer;

class ModuleVideoPlayer
{
    public static function getImageForVideo($vid){
        if(!$vid)
            return '';
        $data = file_get_contents("https://www.googleapis.com/youtube/v3/videos?key=AIzaSyBI-FLS7qnEjV3_1UzBKv9xJeVCbo7nLoM&part=snippet&id=".$vid);
        $json = json_decode($data, true);
        $width = 0;
        $image = false;
        foreach($json['items'][0]['snippet']['thumbnails'] as $key => $thumb){
            if($thumb["width"] >= $width){
                $width = $thumb["width"];
                $image = $thumb["url"];
            }
        };
        return $image;
    }

    static public function getVideoId($link){
        if(!$link)
            return false;
        $video_url = parse_url($link);
        $video_id = '';
        if(in_array($video_url['host'], ['www.youtube.com', 'youtube.com'])){
            parse_str($video_url['query'], $params);
            $video_id = $params['v'];
        }elseif($video_url['host']=='youtu.be'){
            $video_id = trim('/', $video_url['path']);
        }
        return $video_id;
    }

    public static function videoLinkForIframe($link, array $options = []){
        if(!$link)
            return false;
        $options = http_build_query(array_merge(['features'=>'oembed'], $options));
        return "https://www.youtube.com/embed/".self::getVideoId($link).( $options ? "?".$options : '' );
    }
}